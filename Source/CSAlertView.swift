//
//  CSAlertView.swift
//  CSAlertView
//
//  Created by iamaunz on 12/8/2558 BE.
//  Copyright © 2558 iamaunz. All rights reserved.
//

import UIKit
import Foundation

public class CSAlertView :UIView
{
    var sc          = CGSizeZero
    
    
    var title       = ""
    var message     = ""
    var buttons     = []
    var image       = UIImage()
    var view        = UIView()
    var dialog      = UIView()
    
    let width       = CGFloat(280)
    
    convenience init () {
        // SCREEN SIZE
        self.init(frame:CGRect.zero)
        self.sc = UIScreen.mainScreen().bounds.size
        self.setTransformView()
        self.setDialog()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        //self.setCSTextField(frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    func setTransformView() {
        self.view.frame = UIScreen.mainScreen().bounds
    }
    
    func setDialog() {
        let height = CGFloat(200)
        self.dialog = UIView(frame: CGRectMake((sc.width-width)/2, (sc.height-height)/2, width, height))
        self.dialog.backgroundColor = UIColor.whiteColor()
        self.dialog.layer.cornerRadius = 7
        self.dialog.clipsToBounds = true
        self.setTitleView()
        self.setMessageView()
        self.setButtonViews()
    }
    
    func setTitleView() {
        if (self.title.characters.count > 0) {
            let label = UILabel(frame: CGRectMake(0, 0, width, 44))
            label.font = UIFont(name: "Helvetica-Bold", size: 16)
            label.textAlignment = NSTextAlignment.Center
            label.text = self.title
            self.dialog.addSubview(label)
        }
    }
    
    func setMessageView() {
        if (self.message.characters.count > 0) {
            let label = UILabel()
            label.lineBreakMode = NSLineBreakMode.ByTruncatingTail
            label.font = UIFont(name: "Helvetica", size: 16)
            label.textAlignment = NSTextAlignment.Center
            label.text = self.message
            label.numberOfLines = 0
            
            let height = label.sizeThatFits(CGSize(width: width-20, height: 0)).height
            if (self.title.characters.count > 0) {
                label.frame = CGRectMake(10, 44, width-20, height)
            }else{
                label.frame = CGRectMake(10, 0, width-20, height)
            }
    
            label.backgroundColor = UIColor.greenColor()
            self.dialog.addSubview(label)
        }
    }
    
    func setButtonViews() {
        if (self.buttons.count > 0) {
            for (var idx = 0; idx < self.buttons.count; idx++) {
                let w = (width / CGFloat(self.buttons.count))
                let h = CGFloat(44)
                let x = (width / CGFloat(idx+1)) * (CGFloat(idx+1) - 1)
                let y = CGFloat(200-h)
                
                let button = UIButton(type: UIButtonType.System)
                button.addTarget(self, action: "buttonPressed:", forControlEvents: .TouchUpInside)
                button.setTitle(String(self.buttons[idx]), forState: UIControlState.Normal)
                button.frame = CGRectMake(x, y, w, h)
                button.tag = idx
                self.dialog.addSubview(button)
            }
            
            let line = UIView(frame: CGRectMake(0, CGFloat(200-44), width, 1))
            line.backgroundColor = UIColor(red: 198.0/255.0, green: 198.0/255.0, blue: 198.0/255.0, alpha: 1.0)
            self.dialog.addSubview(line)
        }
    }
    
    func buttonPressed(button :UIButton) {
        print("buttonPressed")
        self.close()
    }
    
    public func show() {
        self.setDialog()
        self.view.addSubview(self.dialog)
        UIApplication.sharedApplication().windows.first?.addSubview(self.view)
        
        self.dialog.layer.opacity = 0.0
        self.dialog.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1.0)
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
        UIView.animateWithDuration(0.2) { () -> Void in
            self.dialog.layer.opacity = 1.0
            self.dialog.layer.transform = CATransform3DMakeScale(1, 1, 1)
            self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        }


    }
    
    public func close() {
        self.dialog.layer.opacity = 1.0
        self.dialog.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0)
        self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.dialog.layer.opacity = 1.0
            self.dialog.layer.transform = CATransform3DMakeScale(1.5, 1.5, 1)
            self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.0)
        }) { (finished) -> Void in
            self.removeFromSuperview()
        }
    }
}











