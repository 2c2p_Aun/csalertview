//
//  ViewController.swift
//  CSAlertView
//
//  Created by iamaunz on 12/8/2558 BE.
//  Copyright © 2558 iamaunz. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadScreen()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadScreen() {
        let button = UIButton(type: UIButtonType.Custom)
        button.addTarget(self, action: Selector("displayAlert"), forControlEvents: UIControlEvents.TouchUpInside)
        button.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        button.setTitle("BUTTON", forState: UIControlState.Normal)
        button.backgroundColor = UIColor.purpleColor()
        button.frame = CGRectMake(10, 100, 300, 44)
        self.view.addSubview(button)
    }
    
    func displayAlert() {
        let alertView = CSAlertView()
        alertView.title = "Hello"
        alertView.message = "it's me"
        alertView.buttons = ["OK", "CANCEL"]
        //alertView.image = UIImage(named: "image")!
        alertView.show()
    }

}












